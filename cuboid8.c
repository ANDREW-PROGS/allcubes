/*****************************************************************************/
//26_01_20 simplify saturn name
//declare SPRSYS as char
//correct cubecorner coords, declaring ints as signed ints opened the way. 
//uses Draw triangle and sintab81 code from bll
//courtesy of B.Stick @BS42
//declares MATHX registers as extern int MATHX;
//and .global in
//setexecrot() and theta[axis] declared in same file.
//sintab_8.asm sine table 256 16-bit signed entries (cosine as well)
#include <lynx.h>
#include <tgi.h>
#include <joystick.h>
#include <6502.h>
#include <string.h>
#include <stdlib.h>

extern char lynxtgi[];
extern char lynxjoy[];
extern char SinTablo[];
extern char SinTabhi[];
extern int MATHD; 
extern int MATHH; 
extern int MATHB;
extern int MATHL;
extern int MATHK;
extern int MATHM;
extern char SPRSYS; 
extern void Dtangle(void);
extern char saturn2[]; 
extern int x1;
extern int y1;
extern int x2;
extern int y2;
extern int x3;
extern int y3;
extern char tri_scb2_color;
extern char tri_scb_color;

typedef struct {
    unsigned char b0;
    unsigned char b1;
    unsigned char b2;
    void *next;
    void *bitmap;
    int posx, posy, sizex, sizey;
    int stretch, tilt;
    char palette[8];
} sprite_t;

	sprite_t saturn = {
	0xc5, 0x30, 0x01,
	0,
	&saturn2,
	0, 0,
	0x100, 0x100,
	0x0000, 0x0000,
	{0x01, 0x33, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef}
};

char palette1[] = {
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x02,0x03,0x02,0x02,0x06,0x05,
	0x60,0x80,0x72,0x62,0x93,0x96,0xF5,0xC5,0xB9,0xF9,0x83,0xB3,0xC6,0xF8,0xC3,0xD4};

#define VERTICES 8 //for testing 
// Moving Polygons 
// Proposed 3d environment for Lynx and CC65


typedef struct {
	signed int elemnt1;
	signed int elemnt2;
	signed int elemnt3;
} execrot;

	execrot execrotx;
	execrot execroty;
	execrot execrotz;
	int actual_angle[3];
	char angular[3];
	char vertices = 0x08;
	signed int tripoint[VERTICES*3];
	int xorigin[3];
	int theto[3]; // representation of -pi/2>theta>pi/2
	int	xd[8], yd[8] ; 
	unsigned char axis = 3; 
	unsigned char vertex = 1;
//cube database here
typedef struct {
	signed int xcoord;
	signed int ycoord;
	signed int zcoord;
} cubecorner;

cubecorner v0 = { 0x0010, 0x0010, 0xFFF0};
cubecorner v1 = { 0x0010, 0xFFF0, 0xFFF0};
cubecorner v2 = { 0xFFF0, 0xFFF0, 0xFFF0}; 
cubecorner v3 = { 0xFFF0, 0x0010, 0xFFF0};
cubecorner v4 = { 0x0010, 0xFFF0, 0x0010};
cubecorner v5 = { 0xFFF0, 0xFFF0, 0x0010};
cubecorner v6 = { 0xFFF0, 0x0010, 0x0010};
cubecorner v7 = { 0x0010, 0x0010, 0x0010};


int Sin(char ang,char sgn)
{ 
	int tval;
	tval = (0x100 * SinTabhi[ang]) + SinTablo[ang];
	if (!sgn) { 
	tval = 0xffff ^ tval;
	tval++ ;
}
	return tval;
} 
//similar for cos (different offset!!)
int cos(char ang,char sgn)
{ 
	int tval;
	ang = (ang + 0x40); //ang is char >255 code 10_11_19
	tval = (0x100 * SinTabhi[ang]) + SinTablo[ang];
	if (!(sgn)) {
	tval = 0xffff ^ tval;
	tval++ ;
}
	return tval;
} 

void execmove(signed char distance)
{
	char p;
	int *cubeadd;	
	cubeadd = &v0.zcoord; 
	for (p=0;p < vertices;p++){
	*cubeadd += distance;
	cubeadd +=3;
}	
}	
//////////////////////////////////////////////////////
signed char projectedx (signed int view1, signed int view2)

{	
	char xview,zview;
	if (view2 & 0x8000) {
	view2 = view2 ^ 0xffff;
	view2 ++;
}	
	zview = 24; //value was16 db20_11_19
	xview = (view1 * zview) / (zview + view2); 
	xview = xview + 80;
	//centre origin at middle of screen
	return xview; 
}
signed char projectedy(signed int view1, signed int view2) //dbug 4_8_19
{
	char yview,zview; 
	if (view2 & 0x8000) {
	view2 = view2 ^ 0xffff;
	view2 ++;
}	
	zview = 24;
	yview = (view1 * zview) / (zview + view2);
	yview = yview + 51;
	return yview; 
}

void buildcube()
{
	//build facets
	int *point3; 
	char j;
	point3 = &v0.xcoord;
for (j = 0;j < vertices;j++) 
{
	xd[j] = projectedx(*point3,*(point3+2));
	yd[j] = projectedy( *(point3+1),*(point3+2));// edit 4_8_19
	point3 += 3;
}
}	
void vport()
{
x1 = xd[0];
y1 = yd[0];
x2 = xd[1];
y2 = yd[1];
x3 = xd[2];
y3 = yd[2];
Dtangle();
x1 = xd[2];
y1 = yd[2];
x2 = xd[3];
y2 = yd[3];
x3 = xd[0];
y3 = yd[0];
Dtangle();
asm ("lda _tri_scb2_color");
asm ("inc");
asm ("sta _tri_scb2_color");
asm ("sta _tri_scb_color");
x1 = xd[0];
y1 = yd[0];
x2 = xd[1];
y2 = yd[1];
x3 = xd[7];
y3 = yd[7];
Dtangle(); 
x1 = xd[7];
y1 = yd[7];
x2 = xd[4];
y2 = yd[4];
x3 = xd[1];
y3 = yd[1];
Dtangle();
asm ("lda _tri_scb2_color");
asm ("inc");
asm ("sta _tri_scb2_color");
asm ("sta _tri_scb_color");
x1 = xd[1];
y1 = yd[1];
x2 = xd[2];
y2 = yd[2];
x3 = xd[4];
y3 = yd[4];
Dtangle();
x1 = xd[2];
y1 = yd[2];
x2 = xd[5];
y2 = yd[5];
x3 = xd[4];
y3 = yd[4];
Dtangle(); 
asm ("lda _tri_scb2_color");
asm ("inc");
asm ("sta _tri_scb2_color");
asm ("sta _tri_scb_color");
x1 = xd[2];
y1 = yd[2];
x2 = xd[3];
y2 = yd[3];
x3 = xd[6];
y3 = yd[6];
Dtangle();
x1 = xd[5];
y1 = yd[5];
x2 = xd[2];
y2 = yd[2];
x3 = xd[6];
y3 = yd[6];
Dtangle();
asm ("lda _tri_scb2_color");
asm ("inc");
asm ("sta _tri_scb2_color");
asm ("sta _tri_scb_color");
x1 = xd[4];
y1 = yd[4];
x2 = xd[7];
y2 = yd[7];
x3 = xd[5];
y3 = yd[5];
Dtangle();
x1 = xd[5];
y1 = yd[5];
x2 = xd[7];
y2 = yd[7];
x3 = xd[6];
y3 = yd[6];
Dtangle();
asm ("lda _tri_scb2_color");
asm ("inc");
asm ("sta _tri_scb2_color");
asm ("sta _tri_scb_color");
x1 = xd[7];
y1 = yd[7];
x2 = xd[0];
y2 = yd[0];
x3 = xd[3];
y3 = yd[3];
Dtangle(); 
x1 = xd[3];
y1 = yd[3];
x2 = xd[6];
y2 = yd[6];
x3 = xd[7];
y3 = yd[7];
Dtangle();
}



void setexecrot(char theta) 
//axis should be in scope.
{
	switch(axis)
{
	case (2) : { // data is for rotate about y axis.
	execrotx.elemnt1 = cos(theta,1);
	execrotx.elemnt2 = 0;
	execrotx.elemnt3 = Sin(theta,1);  
	execroty.elemnt1 = 0;
	execroty.elemnt2 = 0x100;
	execroty.elemnt3 = 0;  
	execrotz.elemnt1 = Sin(theta,0);
	execrotz.elemnt2 = 0;
	execrotz.elemnt3 = cos(theta,1);  
} 
	break ; //or return 
	case (1) : { // about the x axis.
	execrotx.elemnt1 = 0x100;
	execrotx.elemnt2 = 0;
	execrotx.elemnt3 = 0; 
	execroty.elemnt1 = 0;
	execroty.elemnt2 = cos(theta,1);
	execroty.elemnt3 = Sin(theta,0);
	execrotz.elemnt1 = 0;
	execrotz.elemnt2 = Sin(theta,1);
	execrotz.elemnt3 = cos(theta,1);  
}
	break ; //or return 
	case (3) : { //about the z axis.
	execrotx.elemnt1 = cos(theta,1);
	execrotx.elemnt2 = Sin(theta,0);
	execrotx.elemnt3 = 0; 
	execroty.elemnt1 = Sin(theta,1);
	execroty.elemnt2 = cos(theta,1);
	execroty.elemnt3 = 0;  
	execrotz.elemnt1 = 0;
	execrotz.elemnt2 = 0;
	execrotz.elemnt3 = 0x100; 
}
	break ; //or return 
}
	return ;
}

//I include checks for multiply by zero.

void multmatrix(char vertex)
{
int tempx = tripoint[vertex], tempy = tripoint[vertex+1], tempz = tripoint[vertex+2];
//copy to temp values
// Initialise the accumulator 0xJKLM
//by writing a 0 to K and M.
// 16bit answer given by contents of 0xKL 
//the multiply and accumulate -maths

	SPRSYS = SPRSYS | 0xc0; //turn acc. and signed math on
	MATHM = 0x0; 
	MATHK = 0x0;
	if (execrotx.elemnt1) {
	MATHD = execrotx.elemnt1;
	MATHB = tempx; //calculation starts with write to _MATHA
}
	while (tgi_busy()) { 
}
	if (execrotx.elemnt2) {
	MATHD = execrotx.elemnt2;
	MATHB = tempy; //calculation starts with write to _MATHA
}
	while (tgi_busy()) { 
}
	if (execrotx.elemnt3) {
	MATHD = execrotx.elemnt3;
	MATHB = tempz; //calculation starts with write to _MATHA
}
	while (tgi_busy()) { 
}
	tripoint[vertex] = MATHL;

//  0xJKLM is contiguous and in proper longword order in hardware regs.
	
	MATHM = 0x0000;
	MATHK = 0x0000;
	// initialise accumulator (JKLM)
	
	if (execroty.elemnt1) {
	MATHD = execroty.elemnt1;
	MATHB = tempx;
}
	while (tgi_busy()) { 
}
	//MATHB = tempx; //calculation starts with write to _MATHA
	if (execroty.elemnt2) {
	MATHD = execroty.elemnt2;
	MATHB = tempy; //calculation starts with write to _MATHA
}
	while (tgi_busy()) { 
}
	if (execroty.elemnt3) {
	MATHD = execroty.elemnt3;
	MATHB = tempz; //calculation starts with write to _MATHA
}
	while (tgi_busy()) { 
}
	tripoint[vertex+1] = MATHL; //new y calc'd
// initialise accumulator (JKLM)
	MATHM = 0x0000;
	MATHK = 0x0000;
	if (execrotz.elemnt1) {
	MATHD = execrotz.elemnt1;
	MATHB = tempx; //calculation starts with write to _MATHA
}
	while (tgi_busy()) { 
}
	if (execrotz.elemnt2) {
	MATHD = execrotz.elemnt2;
	MATHB = tempy; //calculation starts with write to _MATHA
}
	while (tgi_busy()) { 
}
	if (execrotz.elemnt3) {
	MATHD = execrotz.elemnt3;
	MATHB = tempz; //calculation starts with write to _MATHA
}
	while (tgi_busy()) { 
}
	tripoint[vertex+2] = MATHL; //new z calc'd
	SPRSYS = SPRSYS & 0x3f; // turn accumulator and signed math off
}
void main()
{
	char l;
	char j; //debug j variable used elsewhere
	int *magp;
	tgi_install(&lynx_160_102_16);
	tgi_install(&lynxtgi);
    joy_install(&lynx_stdjoy);
     tgi_init();
     CLI();

	tgi_setpalette(palette1); 
   	tgi_setcolor(3);
	tgi_bar(0,0,159,101);  
	while (tgi_busy()){   
}
	tgi_updatedisplay(); // swapbuffers
/***************************************/
	setexecrot(32); //was 0,ed 20_11_19
	magp = &v0.xcoord; // magp is magic pointer :)
	for (l =0;l < 23;l +=3){
	tripoint[l] = *magp;
	tripoint[l+1] = *(magp + 1);
	tripoint[l+2] = *(magp + 2);
	multmatrix(l);
	magp +=3;	
}
	axis = 1; // about <x-axis>, prev 2<y-axis>.
	setexecrot(32); 
	for (l =0;l < 23;l +=3){
	multmatrix(l);
}	
	execmove(-24);
	magp = &v0.xcoord;
	for (l =0;l < 23;l +=3){
	tripoint[l+2] = *(magp + 2);
	magp +=3;	
}
/********************************
/place tripoint values in x1-y3
***********************************/
	l = 0;
	for (j =0;j < 8;j++){
	// terminal value 8 for 8 code vertices 
	xd[j] = projectedx(tripoint[l], tripoint[l+2]); 
	yd[j] = projectedy(tripoint[l+1], tripoint[l+2]); 
	l +=3;
}		
	tgi_setcolor(0); 
	tgi_bar(0,0,159,101); // clear screen 
	while (tgi_busy()){
}
	tgi_updatedisplay(); // swapbuffers	
	saturn.posx = 0x0;
	saturn.posy = 0x0;
	saturn.sizex = 0x100;
	saturn.sizey = 0x100;
	tgi_sprite(&saturn);
	vport(); 
	for(;;){
}
}



