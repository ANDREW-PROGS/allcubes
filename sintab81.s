;* sine-table : 8 bit fix-point
;* assemble with option -d
             ;  run 0
; orig B.Stick/bll
; converted for ra65 15/3/2013

	.global _SinTablo,_SinTabhi
.segment "RODATA"  
_SinTablo:
.byte $00,$06,$0D,$13,$19,$1F,$26,$2C
.byte $32,$38,$3E,$44,$4A,$50,$56,$5C
.byte $62,$68,$6D,$73,$79,$7E,$84,$89
.byte $8E,$93,$98,$9D,$A2,$A7,$AC,$B1
.byte $B5,$B9,$BE,$C2,$C6,$CA,$CE,$D1
.byte $D5,$D8,$DC,$DF,$E2,$E5,$E7,$EA
.byte $ED,$EF,$F1,$F3,$F5,$F7,$F8,$FA
.byte $FB,$FC,$FD,$FE,$FF,$FF,$00,$00
.byte $00,$00,$00,$FF,$FF,$FE,$FD,$FC
.byte $FB,$FA,$F8,$F7,$F5,$F3,$F1,$EF
.byte $ED,$EA,$E7,$E5,$E2,$DF,$DC,$D8
.byte $D5,$D1,$CE,$CA,$C6,$C2,$BE,$B9
.byte $B5,$B1,$AC,$A7,$A2,$9D,$98,$93
.byte $8E,$89,$84,$7E,$79,$73,$6D,$68
.byte $62,$5C,$56,$50,$4A,$44,$3E,$38
.byte $32,$2C,$26,$1F,$19,$13,$0D,$06
.byte $00,$FA,$F3,$ED,$E7,$E1,$DA,$D4
.byte $CE,$C8,$C2,$BC,$B6,$B0,$AA,$A4
.byte $9E,$98,$93,$8D,$87,$82,$7C,$77
.byte $72,$6D,$68,$63,$5E,$59,$54,$4F
.byte $4B,$47,$42,$3E,$3A,$36,$32,$2F
.byte $2B,$28,$24,$21,$1E,$1B,$19,$16
.byte $13,$11,$0F,$0D,$0B,$09,$08,$06
.byte $05,$04,$03,$02,$01,$01,$00,$00
.byte $00,$00,$00,$01,$01,$02,$03,$04
.byte $05,$06,$08,$09,$0B,$0D,$0F,$11
.byte $13,$16,$19,$1B,$1E,$21,$24,$28
.byte $2B,$2F,$32,$36,$3A,$3E,$42,$47
.byte $4B,$4F,$54,$59,$5E,$63,$68,$6D
.byte $72,$77,$7C,$82,$87,$8D,$93,$98
.byte $9E,$A4,$AA,$B0,$B6,$BC,$C2,$C8
.byte $CE,$D4,$DA,$E1,$E7,$ED,$F3,$FA
_SinTabhi:
.byte $00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$01,$01
.byte $01,$01,$01,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$FF,$FF,$FF,$FF,$FF,$FF,$FF
.byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
.byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
.byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
.byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
.byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
.byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
.byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
.byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
.byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
.byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
.byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
.byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
.byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
.byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
.byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
