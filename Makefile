################ EDIT THESE ########################
RODATA_SEGMENT=RODATA

target = game

# These are the object files we want this makefile to produce
objects= \
	lynx-stdjoy.o \
	lynx-160-102-16.o \
	sintab81.o \
	Dt7.o \
	saturn2.o \
	cuboid8.o \

############### DONT EDIT BELOW THIS ###############  

# We are compiling for Atari Lynx
SYS=lynx

# These are the names of the tools we use
CL=cl65
CO=co65
CC=cc65
AS=ca65
AR=ar65
SPRPCK=sprpck
CP=cp
RM=del /F
ECHO=echo
TOUCH=touch
LYNXER=lynxer
MAKELNX=make_lnx

# The flag for adding stuff to a library
ARFLAGS=a

# The flags for compiling C-code
CFLAGS=-I . -t $(SYS) --add-source -O -Or -Cl -Os

# Include paths we may need for compilations
ifeq ($(CC65_INC),)
	CC65_INC=/usr/lib/cc65/include
endif
ifeq ($(CC65_ASMINC),)
	CC65_ASMINC="$(CC65_INC)/../asminc"
endif

# Rule for making a *.o file out of a *.c file
%.o: %.c
	$(CC) $(CFLAGS) -o $(patsubst %c, %s, $(notdir $<)) $<
	$(AS) -o $@ $(AFLAGS) $(*).s
#	$(RM) $*.s

# Rule for making a *.o file out of a *.s file
%.o: %.s
	$(AS) -t lynx -I $(CC65_ASMINC) -o $@ $(AFLAGS) $<
	
# Rule for making a *.o file out of a *.bmp file
%.o : %.bmp
	$(SPRPCK) -t6 -p2 $<
#	$(SPRPCK) -t6 -p2 $< -works kinda 22_1_20
	$(ECHO) .global _$* > $*.s
	$(ECHO) .segment \"$(RODATA_SEGMENT)\" >> $*.s
	$(ECHO) _$*: .incbin \"$*.spr\" >> $*.s
#	$(ECHO) _$*: .incbin \"$*.obj\" >> $*.s
	$(AS) -t lynx -o $@ $(AFLAGS) $*.s
	$(RM) $*.s
	$(RM) $*.pal
	$(RM) $*.spr

#saturn2.o : saturn2.bmp
#	$(SPRPCK) -t6 -p0 -S160102 $< 
#	$(SPRPCK) -t6 -p0 -S160102 -i160102 -a000000 $< 
#	$(ECHO) .global _saturn3 > $*.s
#	$(ECHO) .segment \"$(RODATA_SEGMENT)\" >> $*.s 
#	$(ECHO) _saturn3: .incbin \"saturn3.obj\" >> $*.s 
#	$(AS) -t lynx -o $@ $(AFLAGS) $*.s

all: $(target)

lynx-stdjoy.o:
	$(CP) "$(CC65_INC)/../joy/$*.joy" .
	$(CO) --code-label _lynxjoy $*.joy
	$(AS) -t lynx -o $@ $(AFLAGS) $*.s
	$(RM) $*.joy
	$(RM) $*.s

lynx-160-102-16.o:
	$(CP) "$(CC65_INC)/../tgi/$*.tgi" .
	$(CO) --code-label _lynxtgi $*.tgi
	$(AS) -t lynx -o $@ $(AFLAGS) $*.s
	$(RM) $*.tgi
	$(RM) $*.s

$(target) : $(objects)
	$(CL) -t $(SYS) -o $@.O -m $(target).map -C lynx-coll.cfg $(objects) lynx.lib 
	$(LYNXER) game.O
	$(MAKELNX) game.LYX -b0 256k

clean :
	$(TOUCH) $(objects)
	$(RM) $(objects)
	$(TOUCH) objlist
	$(RM) objlist
	$(TOUCH) game.O
	$(RM) game.O
	$(TOUCH) game.LYX
	$(RM) game.LYX
	$(TOUCH) game.lnx
	$(RM) game.lnx
	$(TOUCH) game.map
	$(RM) game.map

